FROM erlang:alpine
MAINTAINER sebisnow

# elixir expects utf8.
ENV ELIXIR_VERSION="v1.9.0" \
	LANG=C.UTF-8

RUN set -xe \
	&& ELIXIR_DOWNLOAD_URL="https://github.com/elixir-lang/elixir/archive/${ELIXIR_VERSION}.tar.gz" \
	&& ELIXIR_DOWNLOAD_SHA256="dbf4cb66634e22d60fe4aa162946c992257f700c7db123212e7e29d1c0b0c487" \
	&& buildDeps=' \
		ca-certificates \
		curl \
		make \
	' \
	&& apk add --no-cache --virtual .build-deps $buildDeps \
	&& curl -fSL -o elixir-src.tar.gz $ELIXIR_DOWNLOAD_URL \
	&& echo "$ELIXIR_DOWNLOAD_SHA256  elixir-src.tar.gz" | sha256sum -c - \
	&& mkdir -p /usr/local/src/elixir \
	&& tar -xzC /usr/local/src/elixir --strip-components=1 -f elixir-src.tar.gz \
	&& rm elixir-src.tar.gz \
	&& cd /usr/local/src/elixir \
	&& make install clean \
	&& apk del .build-deps

CMD ["iex"]

RUN set -x && \
    apk --update add git make gcc g++ nodejs nodejs-npm && \
    update-ca-certificates && \
    npm config set strict-ssl=false && \
    npm install npm@latest -g && \
    npm cache clean --force && rm -rf ~/.node-gyp /tmp/* && rm -rf /root/nodejs && \
    rm -rf /var/cache/apk/*

RUN mix local.hex --force && \
    mix local.rebar --force && \
    mix archive.install hex phx_new --force
