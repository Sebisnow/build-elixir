## Development image for Elixir with Phoenix

This image, based on Alpine Linux, encapsulates Elixir1.4.1 and is ready for new Phoenix1.3 or greater with phx.new already pre-installed

Use this image as the base image for your new blazing fast and reliable Elixir Microservices! This image should not be used for deployment as manny unneccessary files reside in it (such as Elixir, npm, mix,...)
